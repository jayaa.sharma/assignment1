const UserModel = require("../models/users.model");
const CommentModel = require("../models/comments.model");

class CommentController {
  //Store a comment by a user
  add = async (req, res) => {
    try {
      const { text, userId } = req.body;

      const userRow = await UserModel.findById(userId);
      if (!userRow) {
        return res.status(404).json({ error: "User does not exist" });
      }

      const addComment = new CommentModel({
        text,
        author: userRow._id,
      });

      //Saving the comment in Comment model
      await addComment.save();

      const response = {
        status: "Success",
        code: "201",
        message: "Comment added by user successfully",
        data: addComment,
      };

      res.status(201).json(response);
    } catch (error) {
      const response = {
        status: "Failed",
        code: "500",
        message: "Failed to add comment",
      };
      res.status(500).json(response);
    }
  };

  //Store a reply to a comment or to reply itself by a user.
  addReply = async (req, res) => {
    try {
      const { commentId } = req.params;
      const { userId, text } = req.body;
      const userRow = await UserModel.findById(userId);
      if (!userRow) {
        return res.status(404).json({ error: "User does not exists" });
      }

      const addReply = await CommentModel.findOneAndUpdate(
        { _id: commentId },
        {
          $push: {
            replies: {
              user: userId,
              text,
            },
          },
        }
      );

      const response = {
        status: "Success",
        code: "201",
        message: "Reply added by user successfully",
      };
      res.status(201).json(response);
    } catch (error) {
      const response = {
        status: "Failed",
        code: "500",
        message: "Failed to add reply",
      };
      res.status(500).json(response);
    }
  };

  //Get all comments by a user
  getList = async (req, res) => {
    try {
      const { userId } = req.params;
      const userRow = await UserModel.findById(userId);
      if (!userRow) {
        return res.status(404).json({ error: "User does not exist" });
      }

      const comments = await CommentModel.find(
        { author: userId },
        {
          text: 1,
        }
      );
      const response = {
        status: "Success",
        code: "201",
        message: "All user comments fetched successfully",
        data: comments,
      };

      res.status(201).json(response);
    } catch (error) {
      const response = {
        status: "Failed",
        code: "500",
        message: "Failed to fetch the comments by user",
      };
      res.status(500).json(response);
    }
  };

  //Get all replies of a comment or a reply.
  getReplyList = async (req, res) => {
    try {
      const { commentId } = req.params;

      const comments = await CommentModel.findOne(
        { _id: commentId },
        {
          "replies.text": 1,
          _id: 0,
        }
      );

      const response = {
        status: "Success",
        code: "201",
        message: "Replies fetched successfully",
        data: comments.replies,
      };

      res.status(201).json(response);
    } catch (error) {
      const response = {
        status: "Failed",
        code: "500",
        message: "Failed to fetch the reply of comments by user",
      };
      res.status(500).json(response);
    }
  };

  //Update comment
  update = async (req, res) => {
    try {
      const { commentId } = req.params;

      const { text } = req.body;

      const updateComment = await CommentModel.findOneAndUpdate(
        { _id: commentId },
        { text },
        { new: true }
      );

      const response = {
        status: "Success",
        code: "201",
        message: "Comment updated successfully",
        data: updateComment,
      };

      res.status(201).json(response);
    } catch (error) {
      const response = {
        status: "Failed",
        code: "500",
        message: "Failed to update comment by user",
      };
      res.status(500).json(response);
    }
  };

  //Delete comment
  delete = async (req, res) => {
    try {
      const { commentId } = req.params;

      const deleteComment = await CommentModel.deleteOne({ _id: commentId });

      const response = {
        status: "Success",
        code: "201",
        message: "Comment deleted successfully",
      };

      res.status(201).json(response);
    } catch (error) {
      console.log(error);
      const response = {
        status: "Failed",
        code: "500",
        message: "Failed to delete comment by user",
      };
      res.status(500).json(response);
    }
  };

  //Update reply
  updateReply = async (req, res) => {
    try {
      const { commentId, replyId } = req.params;
      const { text } = req.body;

      const updateReply = await CommentModel.findOneAndUpdate(
        { _id: commentId, "replies._id": replyId },
        { $set: { "replies.$.text": text } },
        { new: true }
      );
      const response = {
        status: "Success",
        code: "201",
        message: "Reply updated successfully",
        data: updateReply,
      };

      res.status(201).json(response);
    } catch (error) {
      console.log(error);
      const response = {
        status: "Failed",
        code: "500",
        message: "Failed to update reply by user",
      };
      res.status(500).json(response);
    }
  };

  //Delete reply
  deleteReply = async (req, res) => {
    try {
      const { commentId, replyId } = req.body;

      const deleteComment = await CommentModel.updateOne(
        { _id: commentId },
        { $pull: { replies: { _id: replyId } } }
      );

      const response = {
        status: "Success",
        code: "201",
        message: "Reply deleted successfully",
      };

      res.status(201).json(response);
    } catch (error) {
      const response = {
        status: "Failed",
        code: "500",
        message: "Failed to delete reply by user",
      };
      res.status(500).json(response);
    }
  };
}

module.exports = new CommentController();
