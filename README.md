Task Description:
Objective: Develop a small server application using Express and MongoDB/PostgreSQL to
store and serve the data for comments and their authors.
Datakeys required for Users are name, email, gender.
Datakeys required for Comments are text, author, replies, date.
APIs are required to fulfill the following functionalities:

1. Store a comment by a user : DONE
2. Store a reply to a comment or to reply itself by a user : DONE
3. Get all comments by a user : DONE
4. Get all replies of a comment or a reply : DONE
5. Update/Delete comment or a reply to a comment/reply : DONE

a) I have added 2 users in mongodb database and using those users i have performed all operations
b) Added postman collection
c) Added mongo db dump of user and comments
d) Please do npm install
e) nodemon
