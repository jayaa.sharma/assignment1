const bodyParser = require("body-parser");
const express = require("express");
const mongoose = require("mongoose");
const commentRoutes = require("./routes/comments.routes");

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// MongoDB connection database name comments-app
const DB_URL = process.env.DB_URL || "mongodb://localhost:27017/comments-app";
mongoose.connect(DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

//Comment routes
app.use("/api/comment", commentRoutes);

// Server setup
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
